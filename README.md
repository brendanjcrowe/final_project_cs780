# Final_Project_CS780

In this repository you will find all code and plots related to my cs780 Final project. The bulk of the code is in jupyter notebooks.
I apologize if it is messy. I did my best to title the notebooks and .py files to the best of my ability.

To see all the model and their results look at results.ipynb. Most of the models run in that final were trained either on google Colab, or in a different notebook. 

To run code from my notebooks: I have included a serialized version of my working conda environment in environment.yaml.
I would suggest cloning this environment to run code as my dependencies are wacky.

To run code in the Colab notebooks: I am not sure about this they should be able to run on my environment but im not 100% sure.